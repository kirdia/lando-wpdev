#!/bin/bash -e
clear

echo "================================================================="
echo "Awesome WordPress Installer!!"
echo "================================================================="

echo "Installing WordPress plugins..."
WPPLUGINS=(
  "debug-bar"
  "debug-bar-constants"
  "debug-bar-list-dependencies"
  "debug-bar-post-types"
  "debug-bar-shortcodes"
  "developer"
  "query-monitor"
  "custom-post-type-ui"
  "post-types-order"
  "svg-support"
  "theme-check"
  "timber-library"
)
wp plugin install "${WPPLUGINS[@]}" --activate

#

echo "================================================================="
echo "Developer plugin installation is complete."
echo "================================================================="
