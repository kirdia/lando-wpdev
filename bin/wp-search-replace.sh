#!/bin/bash

wp search-replace "$1" 'wp-bulletnew.com' --dry-run --skip-columns=guid

wp search-replace 'wp-bullet.com' 'wp-bulletnew.com' --skip-columns=guid

wp search-replace 'http%3A%2F%2F' 'https%3A%2F%2F' --dry-run --skip-columns=guid

wp search-replace 'http%3A%2F%2Fdomain.com' 'http%3A%2F%2Fnewdomain.com' --skip-columns=guid

wp search-replace 'http:\/\/domain.com' 'http:\/\/newdomain.com' --dry-run --skip-columns=guid

wp search-replace 'http:\/\/domain.com' 'http:\/\/newdomain.com'--skip-columns=guid
