#!/bin/bash -e
clear

echo "================================================================="
echo "Awesome WordPress Installer!!"
echo "================================================================="

echo "Installing WordPress cli packages..."
WP_CLI_PACKAGES=(
  "git@github.com:wp-cli/doctor-command.git"
  "git@github.com:wp-cli/profile-command.git"
  "wp-cli/restful"
  "markri/wp-sec"
)
for pack in "${WP_CLI_PACKAGES[@]}"; do
  wp package install "$pack"
done

echo "================================================================="
echo "Wp cli packages installation is complete."
echo "================================================================="
