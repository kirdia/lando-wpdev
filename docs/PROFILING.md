# Profiling

The Makefile provides various targets to setup php profilers for your app.

Supported profilers
- blackfire [Tideways xhprof](https://blackfire.io/)
- xhprof (tideways) [Tideways xhprof](https://github.com/tideways/php-xhprof-extension)


---

### Blackfire

Get your api keys for your account from [integrations/docker](https://blackfire.io/docs/integrations/docker)


#### Install blackfire profiler

1. Copy api keys from above link to .env file
1. Run `make blackfire-install`

Results:
- Spins up blackfire agent container
- Installs blackfire php module/extension in php container/appserver
- Restarts php/appserver container

#### Run blackfire profiler

1. Run `make blackfire-run`

or install blackfire chrome extension to profile a specific page

https://chrome.google.com/webstore/detail/blackfire-profiler/miefikpgahefdbcgoiicnmpbeeomffld

#### Uninstall blackfire profiler
1. Run `make blackfire-uninstall`

---
### Tideways xhprof


#### Install tideways xhprof profiler and xhgui

1. Run `make xhprof-install`
1. Run `make xhprof-enable`

Results:
- Spins up xhgui and mongo containers
- Installs tideways xhprof php module/extension in php container/appserver
- Restarts php/appserver container
- Adds xhgui configuration file to php container/appserver
- Adds xhprof configuration to public/index.php in php container/appserver

#### Run xhprof profiler

Profiling will run based on the configuration here conf/xhgui/config.default.php

#### Uninstall xhprof profiler
1. Run `make xhprof-uninstall`

---
### Newrelic agent


#### Install newrelic agent

1. Run `make newrelic-install`
1. Configure newrelic agent by running `sudo newrelic-install install` inside appserver container


#### Run newrelic agent

No need to run it manually. When it is installed and configured it will send the analytics straight to newrelic

#### Uninstall newrelic agent
1. Run `make newrelic-uninstall`


---
#### TODO
- Add blackfire profiler support [Blackfire](https://blackfire.io/)
