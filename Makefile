# include optional configuration (used for NRO configuration)
-include Makefile.include
-include .env
export

SHELL := /bin/bash

DOCKER_COMPOSE_FILE ?= docker-compose.yml

WP_ADMIN_USER := admin
WP_ADMIN_PASS := admin

WP_USER ?= $(shell whoami)
WP_USER_EMAIL ?= $(shell git config --get user.email)


PROJECT ?= $(shell basename "$(PWD)" | sed 's/[.-]//g')
export PROJECT

# Export Lando project name.
PROJECT_NAME := $(shell grep PROJECT_NAME .lando.env | cut -d'=' -f2)
LANDO_DOMAIN_TLD := $(shell grep LANDO_DOMAIN_TLD .lando.env | cut -d'=' -f2)
PROJECT_DOMAIN = ${PROJECT_NAME}.${LANDO_DOMAIN_TLD}
PROJECT_URL = https://${PROJECT_NAME}.${LANDO_DOMAIN_TLD}

# Lando proxy docker network name
LANDO_PROXY_NET=$(shell docker ps -aqf 'name=^landoproxy.*' | xargs -I{} docker inspect --format='{{range $$k,$$v := .NetworkSettings.Networks}}{{$$k}}{{end}}' {})
# Lando proxy container ip
LANDO_PROXY_IP=$(shell docker ps -aqf 'name=^landoproxy.*' | xargs -I{} docker inspect --format='{{range $$k,$$v := .NetworkSettings.Networks}}{{$$v.IPAddress}}  {{end}}' {})
LANDO_POST_LANDO_FILES=$(lando config --path postLandoFiles --format json | jq -c)

export PROJECT_NAME
export PROJECT_DOMAIN
export PROJECT_URL
export LANDO_PROXY_NET
export LANDO_PROXY_IP
export LANDO_POST_LANDO_FILES

export APPSERVER_CONTAINER_ID=$(shell docker ps -aqf 'name=^${PROJECT_NAME}_appserver.{1,3}$$')
# ============================================================================
#

# Check necessary commands exist

DOCKER := $(shell command -v docker 2> /dev/null)
ENVSUBST := $(shell command -v envsubst 2> /dev/null)
COMPOSER := $(shell command -v composer 2> /dev/null)
SHELLCHECK := $(shell command -v shellcheck 2> /dev/null)
YAMLLINT := $(shell command -v yamllint 2> /dev/null)

# ============================================================================

.DEFAULT_GOAL := all

.PHONY: all
all: build config status
	@echo "Ready"

.PHONY: init
init: .git/hooks/pre-commit

.git/hooks/%:
	@chmod 755 .githooks/*
	@find .git/hooks -type l -exec rm {} \;
	@find .githooks -type f -exec ln -sf ../../{} .git/hooks/ \;

.PHONY: hosts
hosts:
	@if ! grep -q "127.0.0.1[[:space:]]\+${PROJECT_NAME}" /etc/hosts; then \
	cp /etc/hosts hosts.backup; \
	echo "Your hosts file has been backed up to $(PWD)/'hosts.backup'"; \
	echo ""; \
	echo "May require sudo password to configure the /etc/hosts file ..."; \
	echo -e "\n# Planet4 local development environment\n127.0.0.1\t${PROJECT_DOMAIN}" | sudo tee -a /etc/hosts; \
	else echo "Hosts file already configured"; fi

.PHONY: build
build : build-config hosts

.PHONY: build-config
build-config:
	echo "Enter project name"; \
	read -p 'Project name: ' PROJECT_NAME; \
	echo "Creating lando and env conf files"; \
	envsubst < conf/templates/.lando.base.tmpl.yml > .lando.base.yml; \
	envsubst < conf/templates/.lando.tmpl.env > .lando.env; \
	envsubst < conf/templates/.tmpl.env > .env;
	echo '---' > .lando.local.yml
	mkdir -p artifacts

.PHONY: replace-domain
replace-domain: OLD_DOMAIN :=$(shell sed 's~http[s]*://~~g' <<< $$(lando wp option get siteurl))
replace-domain:
	echo ${OLD_DOMAIN}; \
	lando wp search-replace "${OLD_DOMAIN}" "${PROJECT_DOMAIN}" --skip-columns=guid ;
# ============================================================================

# SELF TESTS

.PHONY : lint
lint:
	@$(MAKE) -j lint-docker lint-sh lint-yaml

lint-docker:
ifndef DOCKER
	$(error "docker is not installed: https://docs.docker.com/install/")
endif

lint-sh:
ifndef SHELLCHECK
	$(error "shellcheck is not installed: https://github.com/koalaman/shellcheck")
endif
	@find . ! -path './public/*' -type f -name '*.sh' | xargs shellcheck

lint-yaml:
ifndef YAMLLINT
	$(error "yamllint is not installed: https://github.com/adrienverge/yamllint")
endif
	@find . ! -path './public/*' -type f -name '*.yml' | xargs yamllint -c .yamllint.yml

#lint-ci:
#ifndef CIRCLECI
#	$(error "circleci is not installed: https://circleci.com/docs/2.0/local-cli/#installation")
#endif
#	@circleci config validate >/dev/null

# ============================================================================

.PHONY: env
env:
	@echo export $(COMPOSE_ENV)

# ============================================================================

# CLEAN GENERATED ASSETS

.PHONY : clean
clean:
	./clean.sh


.PHONY : run
run:
	@$(MAKE) -j init getdefaultcontent db/Dockerfile
	cp ci/scripts/duplicate-db.sh defaultcontent/duplicate-db.sh
	@./go.sh
	@./wait.sh

# ============================================================================

# DEVELOPER ENVIRONMENT

.PHONY: dev
dev : clean-dev
	@./dev.sh

dev-install-xdebug:
ifeq (Darwin, $(shell uname -s))
	$(eval export XDEBUG_REMOTE_HOST=$(shell ipconfig getifaddr en0))
else
	$(eval export XDEBUG_REMOTE_HOST=$(shell docker network inspect ${PROJECT}_local --format '{{(index .IPAM.Config 0).Gateway }}'))
endif
ifndef ENVSUBST
	$(error Command: 'envsubst' not found, please install using your package manager)
endif
	docker-compose exec php-fpm sh -c 'apt-get update && apt-get install php-xdebug'
	envsubst < dev-templates/xdebug.tmpl > dev-templates/xdebug.out
	docker cp dev-templates/xdebug.out $(shell $(COMPOSE_ENV) docker-compose ps -q php-fpm):/tmp/20-xdebug.ini
	docker-compose exec php-fpm sh -c 'mv /tmp/20-xdebug.ini /etc/php/$${PHP_MAJOR_VERSION}/fpm/conf.d/20-xdebug.ini'
	docker-compose exec php-fpm sh -c 'service php$${PHP_MAJOR_VERSION}-fpm reload'

dev-flush-caches:
	docker-compose exec php-fpm sh -c 'wp cache flush'
	docker-compose exec php-fpm sh -c 'rm -rf public/wp-content/plugins/timber-library/cache/twig/*'
	docker-compose exec php-fpm sh -c 'wp nginx-helper purge-all'


# ============================================================================

# POST INSTALL AND CONFIGURATION TASKS

.PHONY: config
config:
	docker-compose exec -T php-fpm wp option set rt_wp_nginx_helper_options '$(NGINX_HELPER_JSON)' --format=json
	$(MAKE) flush


.PHONY : pass
pass:
	@make pmapass
	@make wppass

.PHONY : wppass
wppass:
	@printf "Wordpress credentials:\n"
	@printf "User:  %s\n" $(WP_ADMIN_USER)
	@printf "Pass:  %s\n" $(WP_ADMIN_PASS)
	@printf "\n"

.PHONY : pmapass
pmapass:
	@printf "Database credentials:\n"
	@printf "User:  %s\n" $(MYSQL_USER)
	@printf "Pass:  %s\n----\n" $(MYSQL_PASS)
	@printf "User:  root\n"
	@printf "Pass:  %s\n----\n" $(ROOT_PASS)

.PHONY : wpadmin
wpadmin:
	@docker-compose exec -T php-fpm wp user create ${WP_USER} ${WP_USER_EMAIL} --role=administrator

.PHONY: status
status:
	@echo
	@$(MAKE) pass
	@echo
	@echo " Frontend - https://${PROJECT_DOMAIN}"
	@echo " Backend  - https://${PROJECT_DOMAIN}/wp-admin"
	@echo
	@echo "Execute the following command to configure your local shell environment"
	@echo
	@$(MAKE) env
	@echo


# ============================================================================== #




# BACKSTOPJS TASKS
# BROWSERSYNC TASKS
# LOCUST.IO TASKS
# SITESPEED.IO TASKS
# ===============================START======================================== #
.PHONY: flush
flush:
	lando wp cache flush

.PHONY: backup-db
backup-db:
	echo "Backing up database..."
	whoami
	mkdir -p backup/db
	lando db-export
	mv *.sql.gz backup/db/
	echo "Database successfully backed up"

#	./backstop.sh reference
# Run backstop reference pointing the project url
# Takes reference screenshots
.PHONY: backstopr
backstopr:
	cp backstop.json artifacts/backstop.json
	docker run --add-host=${PROJECT_DOMAIN}:${LANDO_PROXY_IP} --network=${LANDO_PROXY_NET} --rm -v $$(pwd)/artifacts:/src backstopjs/backstopjs reference

#	./backstop.sh test
# Run backstop test pointing the project url
# Takes screenshots and compares them to reference screenshots
.PHONY: backstopt
backstopt:
	cp backstop.json artifacts/backstop.json
	docker run --add-host=${PROJECT_DOMAIN}:${LANDO_PROXY_IP} --network=${LANDO_PROXY_NET} --rm -v $$(pwd)/artifacts:/src backstopjs/backstopjs test

# Run backstop crawl to crawl site and generate backstopjs config
.PHONY: backstop-crawl
backstop-crawl:
	lando backstop-crawl  ${PROJECT_URL} --allow-subdomains --ignore-ssl-errors --ignore-robots

.PHONY: browsersync
browsersync:
	 browser-sync start --proxy ${PROJECT_URL} --files "public/wp-content/**/*.css" "public/wp-content/**/*.js"

.PHONY: locust
locust:
	curl https://gist.githubusercontent.com/kirdia/3e3f7f1abf4e80d06d0748673c64a4fa/raw/gistfile1.py > artifacts/locustfile.py
	docker run --add-host=${PROJECT_DOMAIN}:${LANDO_PROXY_IP} --network=${LANDO_PROXY_NET} --rm -v $$(pwd)/artifacts:/locust -p 8089:8089 christianbladescb/locustio --host ${PROJECT_URL}

.PHONY: sitespeed
sitespeed:
	echo "Running sitespeed io tests against site"
	echo ${PROJECT_URL}
	docker run --add-host=${PROJECT_DOMAIN}:${LANDO_PROXY_IP} --network=${LANDO_PROXY_NET}  --rm -v $$(pwd)/artifacts:/sitespeed.io sitespeedio/sitespeed.io:11.4.0 ${PROJECT_URL}
	xdg-open $$(ls -td -- artifacts/sitespeed-result/${PROJECT_DOMAIN}/* | head -n 1)/index.html | exit 0


# ================================END=========================================




# XDEBUG TASKS
# ===============================START========================================




# ================================END=========================================



# XHPROF TASKS
# ===============================START========================================
.PHONY: xhprof-install
xhprof-install:
	$(call install_service,xhprof)
	docker cp conf/xhgui/install-extension.sh ${APPSERVER_CONTAINER_ID}:/tmp/xhprof_install.sh
	docker exec ${APPSERVER_CONTAINER_ID} chmod +x /tmp/xhprof_install.sh
	docker exec ${APPSERVER_CONTAINER_ID} /tmp/xhprof_install.sh
	docker cp conf/xhgui/config.default.php ${APPSERVER_CONTAINER_ID}:/tmp/xhgui/config/config.php
	lando rebuild -s mongo -s xhgui-app -y
	$(call restart_appserver)

.PHONY: xhprof-uninstall
xhprof-uninstall:
	$(call uninstall_service,xhprof)
	docker cp conf/xhgui/uninstall-extension.sh ${APPSERVER_CONTAINER_ID}:/tmp/xhprof_uninstall.sh
	docker exec ${APPSERVER_CONTAINER_ID} chmod +x /tmp/xhprof_uninstall.sh
	docker exec ${APPSERVER_CONTAINER_ID} /tmp/xhprof_uninstall.sh
	$(call restart_appserver)

.PHONY: xhprof-enable
xhprof-enable:
	@echo "Enabling xhprof profiler.."
	@sed -i.bak '/<?php/ r conf/xhgui/add-to-index.php' public/index.php && rm public/index.php.bak
	docker exec ${APPSERVER_CONTAINER_ID} sh -c 'echo "extension=tideways_xhprof.so" >"$$PHP_INI_DIR/conf.d/tideways_xhprof.ini" '
	docker cp conf/xhgui/uninstall-extension.sh ${APPSERVER_CONTAINER_ID}:/tmp/xhprof_uninstall.sh
	$(call restart_appserver)

.PHONY: xhprof-disable
xhprof-disable:
	@echo "Disabling xhprof profiler.."
	@grep -v -x -f conf/xhgui/add-to-index.php public/index.php > public/index.php.bak && mv public/index.php.bak public/index.php
	@docker exec ${APPSERVER_CONTAINER_ID} sh -c 'rm -f "$$PHP_INI_DIR/conf.d/tideways_xhprof.ini" '
	$(call restart_appserver)

.PHONY: xhprof-remove
xhprof-remove: xhprof-uninstall xhprof-disable

# ================================END=========================================


# BLACKFIRE TASKS
# ===============================START========================================

# Spin up blackfire agent container
# Install blackfire extension in php container/appserver
# Restart php/appserver container
.PHONY: blackfire-install
blackfire-install:
	echo "Enabling blackfire profiler"; \
	export APPSERVER_CONTAINER_IP=$$(docker ps -aqf 'name=^${PROJECT_NAME}_appserver.{1,3}$$'); \
	docker run --name="blackfire" --network="${PROJECT_NAME}_default" -d -e BLACKFIRE_SERVER_ID -e BLACKFIRE_SERVER_TOKEN blackfire/blackfire ;\
	docker cp conf/blackfire/install-blackfire-extension.sh $$APPSERVER_CONTAINER_IP:/tmp/blackfire_install.sh ; \
	docker exec $$APPSERVER_CONTAINER_IP chmod +x /tmp/blackfire_install.sh ; \
	docker exec $$APPSERVER_CONTAINER_IP /tmp/blackfire_install.sh ; \
	$(call restart_appserver)

# Remove blackfire agent container
# Remove blackfire extension from php container/appserver
# Restart php/appserver container
.PHONY: blackfire-uninstall
blackfire-uninstall:
	echo "Uninstalling blackfire profiler"; \
	export APPSERVER_CONTAINER_IP=$$(docker ps -aqf 'name=^${PROJECT_NAME}_appserver.{1,3}$$'); \
	export BLACKFIRE_CONTAINER_ID=$$(docker ps -aqf 'name=^blackfire.*'); \
	docker stop $$BLACKFIRE_CONTAINER_ID | xargs docker rm; \
	docker cp conf/blackfire/uninstall-blackfire-extension.sh $$APPSERVER_CONTAINER_IP:/tmp/blackfire_uninstall.sh ; \
	docker exec $$APPSERVER_CONTAINER_IP chmod +x /tmp/blackfire_uninstall.sh ; \
	docker exec $$APPSERVER_CONTAINER_IP /tmp/blackfire_uninstall.sh ; \
	$(call restart_appserver)

# Run blackfire profiling against app's homepage
.PHONY: blackfire-run
blackfire-run:
	docker run -it --rm  \
		--add-host=${PROJECT_DOMAIN}:${LANDO_PROXY_IP} --network=${LANDO_PROXY_NET} \
		-e BLACKFIRE_CLIENT_ID \
		-e BLACKFIRE_CLIENT_TOKEN \
		blackfire/blackfire blackfire \
		curl --insecure ${PROJECT_URL}

# ================================END=========================================


# NEWRELIC TASKS
# ===============================START========================================

# Install newrelic extension in php container/appserver
# Restart php/appserver container
.PHONY: newrelic-install
newrelic-install:
	echo "Enabling newrelic daemon"
	export APPSERVER_CONTAINER_IP=$$(docker ps -aqf 'name=^${PROJECT_NAME}_appserver.{1,3}$$')
	docker cp conf/newrelic/install-newrelic-extension.sh $$APPSERVER_CONTAINER_IP:/tmp/newrelic_install.sh
	docker exec $$APPSERVER_CONTAINER_IP chmod +x /tmp/newrelic_install.sh
	docker exec $$APPSERVER_CONTAINER_IP /tmp/newrelic_install.sh
	$(call restart_appserver)


# ================================END=========================================




# STATIC CODE ANALYSIS TASKS
# ===============================START========================================
.PHONY: phpstan
phpstan:
	echo "Running phpstan static code analysis"
	docker run --rm -v $$(pwd)/public:/app phpstan/phpstan analyse wp-content/themes




# ================================END=========================================


# Middle in the man proxy TASKS
# ===============================START========================================
.PHONY: mitm-install
mitm-install:
	$(call install_service,mitmproxy)
	mkdir -p public/wp-content/mu-plugins
	curl https://gist.githubusercontent.com/kirdia/e86d0a8adabfa23633bc3eadd3e676d3/raw/lando-wpdevkit.php > public/wp-content/mu-plugins/lando-wpdevkit.php
	lando rebuild -s proxy-mitm -y

.PHONY: mitm-uninstall
mitm-uninstall:
	$(call uninstall_service,mitmproxy)
	rm -f public/wp-content/mu-plugins/lando-wpdevkit.php
	lando --clear;

# ================================END=========================================


# VARIOUS TOOLS
# ===============================START========================================
.PHONY: getextip
getextip:
	curl ifconfig.co




# ================================END=========================================
#	@docker run --rm -v "$$(pwd)/conf/tests:/test" bats/bats:latest /test

.PHONY: test
test:
	bats conf/tests

.PHONY: test-debug
test-debug:
	bats conf/tests/general.bats






# ELASTICSEARCH TASKS
# ===============================START========================================
.PHONY: elasticsearch-install
elasticsearch-install:
	$(call install_service,elasticsearch)
	lando rebuild -s elasticsearch -s elastichq -y
	lando wp plugin install --activate elasticpress
	lando wp option update ep_host "http://elastic:secret@elasticsearch:9200/"

.PHONY: elasticsearch-uninstall
elasticsearch-uninstall:
	$(call uninstall_service,elasticsearch)
	lando wp plugin uninstall --deactivate elasticpress

# ================================END=========================================


# REDIS TASKS
# ===============================START========================================
.PHONY: redis-install
redis-install:
	$(call install_service,redis)
	lando rebuild -s redis -s redis-commander -y
	lando wp plugin install --activate wp-redis
	lando wp redis enable
	lando wp cache flush

.PHONY: redis-uninstall
redis-uninstall:
	$(call uninstall_service,redis)
	lando wp plugin uninstall --deactivate wp-redis

# ================================END=========================================

# WEBGRIND TASKS
# ===============================START========================================
.PHONY: webgrind-install
webgrind-install:
	mkdir -p artifacts/xdebug
	$(call install_service,webgrind)
	lando rebuild -s webgrind -y

.PHONY: webgrind-uninstall
webgrind-uninstall:
	$(call uninstall_service,webgrind)
	rm -rf artifacts/xdebug

# ================================END=========================================

testt:
	$(call restart_appserver)


define install_service
	echo "Installing $(1) ..";
	envsubst < conf/templates/.lando.$(1).tmpl.yml > .lando.extra.$(1).yml;
	echo '---' > .lando.local.tmp.yml;
	docker run --rm -v "${PWD}":/workdir mikefarah/yq:3 yq m .lando.local.tmp.yml .lando.extra*  > .lando.local.yml;
	rm -f .lando.local.tmp.yml;
endef

define uninstall_service
	echo "Uninstalling $(1) ..";
	echo '---' > .lando.extra.$(1).yml;
	echo '---' > .lando.local.tmp.yml;
	docker run --rm -v "$$(pwd)":/workdir mikefarah/yq:3 yq m .lando.local.tmp.yml .lando.extra* > .lando.local.yml;
	rm -f .lando.local.tmp.yml .lando.extra.$(1).yml;
	lando --clear;
endef

define restart_appserver
	docker container restart $$(docker ps -aqf 'name=^${PROJECT_NAME}_appserver.{1,3}$$');
endef
