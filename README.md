# Lando Wordpress development Skeleton

Setup a wordpress development environment.

This is a [WordPress development](https://make.wordpress.org) environment based on [Lando](https://docs.devwithlando.io/).


## Requirements

Firstly, check you have all the requirements on your system.
For Linux users, these are either preinstalled or available through your distribution's package manager.

- [docker](https://docs.docker.com/engine/installation/)
- [envsubst](https://stackoverflow.com/questions/23620827/envsubst-command-not-found-on-mac-os-x-10-8/23622446#23622446)
- [git](https://www.git-scm.com/downloads)
- [lando](https://docs.lando.dev/basics/installation.html)
- [make](https://www.gnu.org/software/make/) - Instructions for installing make vary, for OSX users `xcode-select --install` might work.

For OsX and Windows users docker installation already includes docker-compose.
Linux users have to install docker-compose separately and also check the Post-installation steps:

- [docker-compose](https://github.com/docker/compose/releases)
- [post-installation](https://docs.docker.com/install/linux/linux-postinstall/)

#### Extra requirements

- [jq](https://github.com/stedolan/jq/wiki/Installation)
- [shellcheck](https://www.shellcheck.net/)
- [yamllint](http://www.yamllint.com/)

#### Docker image requirements

- [yq latest 3 version](https://hub.docker.com/layers/mikefarah/yq/3.3.4/images/sha256-4768ee07141a85b0baf1ca0b7d25fc0af7ca95ded93b744f7f2af871a691c46b?context=explore)


## Features

* Standalone Development Environment based on Lando, which itself requires Docker
* PHPUnit & PHPCodeSniffer
* NPM & Grunt

## Setup

* Install the latest version of Lando via a [GitHub DMG file](https://github.com/lando/lando/releases). You also need to have Docker installed.
* Clone this repository into a directory of your choice. Navigate to that directory.
* Enter project name in .lando.env file and run
  `make build-config`
* Run `lando start`. When doing this for the first time, it will set the environment up for you, so it will take a bit longer than on subsequent starts.
* Access your site under `https://PROJECT_NAME.lndo.site/`. If you're having trouble connecting, you may be facing the [DNS Rebinding Protection issue](https://docs.devwithlando.io/issues/dns-rebind.html). To fix this, and to ensure you can develop while offline, follow the [Working Offline](https://docs.devwithlando.io/config/proxy.html#working-offline-or-using-custom-domains) steps. In other words, add the following to your host machine's `/etc/hosts` file:
```
127.0.0.1       PROJECT_NAME.lndo.site
```

If this is your very first Lando project, make sure that your system trusts the SSL certificate that Lando generates via: `sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain ~/.lando/certs/lndo.site.pem` You might need to restart your browser to see the change being reflected.

## Local development with lando

Local development is easiest with [lando](https://docs.devwithlando.io).

1. Copy `.wpenv.dev.template` to `.wpenv`

2. Set `qm.localhost` to `127.0.0.1` in your hosts file in case you do not use something like [dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html)

3. Run `lando start`

4. Import a production database with `lando db-import <path-to-dump>`

## Manual Installation

1. Install dependencies: `composer install`

2. Create `.wpenv`  file with variables:
    * `DB_NAME` - Database name
    * `DB_USER` - Database user
    * `DB_PASSWORD` - Database password
    * `DB_HOST` - Database host
    * `WP_ENV` - Set to environment (`development`, `staging`, `production`)
    * `AUTH_KEY`, `SECURE_AUTH_KEY`, `LOGGED_IN_KEY`, `NONCE_KEY`, `AUTH_SALT`, `SECURE_AUTH_SALT`, `LOGGED_IN_SALT`, `NONCE_SALT`

    If you want to automatically generate the security keys (assuming you have wp-cli installed locally) you can use the very handy [wp-cli-dotenv-command](https://aaemnnost.tv/wp-cli-commands/dotenv/):

        wp package install aaemnnosttv/wp-cli-dotenv-command

        wp dotenv salts regenerate

    Or, you can cut and paste from the [Roots WordPress Salt Generator](https://roots.io/salts.html).

3. Set your site vhost document root to `/path/to/qm/web/`


## Usage

* You can customize the environment. Variables placed in a custom `.env` file in the root directory will override similar variables from the `.env.base` file. 
Custom CLI configuration can be set up via a `wp-cli.local.yml` file (taking precedence over `wp-cli.yml`), and even custom Lando configuration is possible via a `.lando.yml` file (taking precedence over `.lando.base.yml`). 
For changes to the Lando configuration or environment variables, you will need to run `lando rebuild` to apply them.


## Tools

#### make
```bash
make backstopr       # Run backstopjs reference, take reference screenshots
make backstopt       # Run backstopjs test, test screenshots before-after
make backstop-crawl  # Run backstopjs crawl, generates backstop.json config
make backup-db       # Backup database
make browser-sync    # Start browser-sync
make locust          # Run load tests on site using locust.io
make flush           # Flush wordpress cache
make sitespeed       # Run web performance tests using sitespeed.io
```



### _Versioned files structure_
```bash
├── bin                 # General script files
├── conf                # General script files
│   ├── php             # Php conf files
│   ├── templates       # Templates for .lando and .env files
├── Makefile
├── README.md
├── wp-cli.yml          # Wp cli config
└── .gitignore
```

### _Unversioned files structure_
```bash
├── backstop_data       # Backstopjs artifacts
├── backup              # Backup dir for db backup etc..
│   ├── db              # Db backups
│   ├── hosts           # Php conf files
├── public              # Wordpress files
├── xdebug              # Xdebug profiling artifacts, mounted to appserver and webgrind services
├── .env                # Environment variables for your app
├── .lando.base.yml     # Base lando conf, copied when running make build-config
├── .lando.env          # Environment variables for the lando configs
├── .lando.yml          # Extended lando conf. Add extra services, configuration
```


## TODO
Lando plugin
- Backups db and move it to backup/db
