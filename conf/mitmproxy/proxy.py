"""
mitmproxy script to block non-safe outgoing requests.
"""
from mitmproxy import http
from mitmproxy import ctx


# Modify this to add hostnames to the whitelist. Script is reloaded by mitmproxy automatically.
hostname_whitelist = [
    "api.wordpress.org",
    "api.genesistheme.com",
    "nginx", # Needed for wp-cron requests.
    "getcomposer.org",
]



def request(flow: http.HTTPFlow) -> None:
    if flow.request.method.casefold() not in ["get", "head", "options"] and flow.request.host not in hostname_whitelist:
        print("External %s request to %s blocked in development" %
              (flow.request.method, flow.request.host))
        flow.kill()
