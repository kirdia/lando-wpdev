#!/usr/bin/env bash

# shellcheck disable=SC2046
echo 'deb http://apt.newrelic.com/debian/ newrelic non-free' | tee /etc/apt/sources.list.d/newrelic.list
wget --retry-connrefused --waitretry=1 -t 5 -O - https://download.newrelic.com/548C16BF.gpg | apt-key add -
apt-get update
apt-get install newrelic-php5 -y
