#!/usr/bin/env bash

# shellcheck disable=SC2046
rm $(php -r "echo ini_get ('extension_dir');")/blackfire.so &&
rm "$PHP_INI_DIR/conf.d/blackfire.ini" &&
rm -rf /tmp/blackfire /tmp/blackfire-probe.tar.gz
