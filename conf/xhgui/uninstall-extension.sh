#!/usr/bin/env bash

# shellcheck disable=SC2034,SC2046
rm $(php -r "echo ini_get ('extension_dir');")/tideways_xhprof.so &&
rm "$PHP_INI_DIR/conf.d/tideways_xhprof.ini" &&
rm -rf /tmp/xhgui
