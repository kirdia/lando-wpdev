#!/usr/bin/env bash

# Install pecl mongo extension
# Install xhprof, compile and configure php to use it
# If you add/edit/remove php libraries you may need to `lando rebuild` and `lando restart`
# Tideways (this version requires PHP 7.0+).
pecl install mongodb && docker-php-ext-enable mongodb
cd /tmp && git clone https://github.com/tideways/php-profiler-extension.git
cd /tmp/php-profiler-extension && phpize && ./configure && make && make install
{
  echo 'extension=tideways_xhprof.so'
  echo 'tideways.collect=full'
  echo 'tideways.monitor=full'
  echo 'tideways.framework=wordpress'
} >"$PHP_INI_DIR/conf.d/tideways_xhprof.ini"
# install xhgui
# we just need the perftools/xhgui-collector but the installation is much easier this way
cd /tmp && rm -rf xhgui && git clone https://github.com/perftools/xhgui.git
cd /tmp/xhgui && php install.php
