#!/usr/bin/env bats

@test "install xhprof profiler and test that xhgui is up" {

  make xhprof-install
  make xhprof-enable

  firstString="citestenv"
  secondString="xhgui-citestenv"
  XHGUI_URL="${PROJECT_URL/$firstString/$secondString}"

  docker ps | grep "${PROJECT_NAME}_xhgui"
  docker ps | grep "${PROJECT_NAME}_mongo"

  docker run --rm \
    --add-host=xhgui-${PROJECT_DOMAIN}:${LANDO_PROXY_IP} \
    --network="${LANDO_PROXY_NET}" \
    appropriate/curl -Ik "${XHGUI_URL}" | head -n 1 | grep 200

  make xhprof-remove
}

