#!/usr/bin/env bats

@test "Containers should be up" {
  docker ps | grep "${PROJECT_NAME}_appserver_nginx"
  docker ps | grep "${PROJECT_NAME}_appserver"
  docker ps | grep "${PROJECT_NAME}_database"
  docker ps | grep "${PROJECT_NAME}_pma"
  docker ps | grep "${PROJECT_NAME}_mailhog"
}

@test "app should be up" {

  run echo $PROJECT_NAME
  [ "$output" = "citestenv" ]

  # Test that app responds.
  docker run --rm \
    --add-host=${PROJECT_DOMAIN}:${LANDO_PROXY_IP} \
    --network="${LANDO_PROXY_NET}" \
    appropriate/curl -Ik "${PROJECT_URL}" | head -n 1 | grep 200

  # Test pma responds.
  firstString="citestenv"
  secondString="pma-citestenv"
  PMA_URL="${PROJECT_URL/$firstString/$secondString}"
  docker run --rm \
    --add-host=pma-${PROJECT_DOMAIN}:${LANDO_PROXY_IP} \
    --network="${LANDO_PROXY_NET}" \
    appropriate/curl -Ik "${PMA_URL}" | head -n 1 | grep 200

}
