#!/usr/bin/env bats

@test "Wordpress should be installed" {
  run lando wp core is-installed
  [ "$status" -eq 0 ]
}

@test "Wordpress config path" {
  run lando wp config path
  [ "$output" = "/app/public/wp-config.php" ]
}

@test "Wordpress config has db parameters" {
  run lando wp config has DB_PASSWORD
  [ "$status" -eq 0 ]

  run lando wp config has DB_USER
  [ "$status" -eq 0 ]

  run lando wp config has DB_HOST
  [ "$status" -eq 0 ]
}

@test "Wordpress responds" {
  lando ssh -s appserver_nginx -c "curl -L localhost" | grep "WordPress"
}
